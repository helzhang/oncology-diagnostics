﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.ML;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;

namespace openCV
{
    class PatRec
    {
        public static SVM machine;
        private static string svm_file = "train_svm";
        private static string dir_onko = @"trainpics/onko";
        private static string dir_not_onko = @"trainpics/notonko";
        private static string dir_arcs = @"arcs";
        private static int area = 600 * 400;

        //Устранение шумов и бинаризация
        public static Image<Gray, Byte> Binary(Image<Bgr, byte> image, int gray_threshold = 50, int gaussian = 9)
        {
            image._SmoothGaussian(gaussian);
            //image._EqualizeHist();
            //CvInvoke.EqualizeHist(image, image);

            //using (Image<Hsv, Byte> hsv = image.Convert<Hsv, Byte>())
            //{
            //    Image<Gray, Byte>[] channels = hsv.Split();
            //    try
            //    {
            //        CvInvoke.InRange(channels[0], new ScalarArray(7), new ScalarArray(120), channels[0]);
            //        channels[0]._Not();       
            //        channels[1]._ThresholdBinary(new Gray(30), new Gray(255));
            //        channels[0]._And(channels[1]);
            //        channels[0]._Not();
            //    }
            //    finally
            //    {
            //        channels[1].Dispose();
            //        channels[2].Dispose();
            //    }


            //    return channels[0];
            //}
            var image_bin = image.Convert<Gray, byte>().ThresholdBinary(new Gray(gray_threshold), new Gray(255));
            return image_bin;
        }

        //Сегментация
        public static List<Rectangle> Segmentation(Image<Gray, byte> binary, Image<Bgr, byte> image)
        {
            List<Rectangle> rects = new List<Rectangle>();
            using (Mat hierachy = new Mat())
            using (VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint())
            {                
                CvInvoke.FindContours(binary, contours, hierachy, RetrType.List, ChainApproxMethod.ChainApproxSimple);
                for (int i = 0; i < contours.Size; i++)
                {
                    //if (CvInvoke.ContourArea(contours[i]) < image.Width * image.Height*0.8)
                    //{
                    Rectangle r = CvInvoke.BoundingRectangle(contours[i]);
                    if (r.Height * r.Width < image.Width * image.Height && r.Height * r.Width > image.Width * image.Height * 0.1)
                    {
                        image.Draw(r, new Bgr(0, 0, 255), 2);
                        rects.Add(r);
                    }
                    //}
                }
            }
            return rects;
        }

        //Вычисление процента площади кристаллической зоны
        public static double CalcCenterProc(string filename)
        {
            var image = new Image<Bgr, Byte>(filename).Resize(0.2, Emgu.CV.CvEnum.Inter.Linear);
            Image<Gray, byte> bin_image;
            List<Rectangle> rects = null;
            for (int i = 100; i <= 200; i++)
            {
                bin_image = PatRec.Binary(image, gray_threshold: i, gaussian: 21);
                rects = PatRec.Segmentation(bin_image, image);
                if (rects.Count == 2) break;
            }
            if (rects != null)
                if (rects.Count == 2)
                {
                    double S1 = Math.PI * rects[0].Width / 2 * rects[0].Height / 2;
                    double S2 = Math.PI * rects[1].Width / 2 * rects[1].Height / 2;
                    if (S1 > S2) return S2 / S1 * 100;
                    else return S1 / S2 * 100;
                }
            return 0;
        }

        //Преобразование изображения в массив
        //public static double[] imgTo1DArray(Image<Gray, byte> img)
        //{
        //    Bitmap bitmap = img.ToBitmap();
        //    ImageConverter imgConvert = new ImageConverter();
        //    byte[] byteArr = (byte[])imgConvert.ConvertTo(bitmap, typeof(byte[]));
        //    double[] doublearr = new double[byteArr.Length];
        //    for (int i = 0; i < byteArr.Length; i++)
        //        doublearr[i] = Convert.ToDouble(byteArr[i]);
        //    return doublearr;
        //}

        //Создание двумерного массива для обучения машины опорных векторов
        //public static TrainArr SetTrainArray(List<Pic> pictures)
        //{
        //    var trainarr = new TrainArr();
        //    trainarr.Labels = new int[pictures.Count];
        //    trainarr.Array = new double[pictures.Count][];
        //    for (int i = 0; i < pictures.Count; i++)
        //        trainarr.Array[i] = new double[cols];

        //    double[] t = new double[cols];

        //    for (int i = 0; i < pictures.Count; i++)
        //    {
        //        trainarr.Labels[i] = pictures[i].Label;

        //        t  = imgTo1DArray(Binary(new Image<Bgr, Byte>(pictures[i].Filename)));
        //        for (int j = 0; j < trainarr.Array.Length; j++)
        //        {
        //            trainarr.Array[i][j] = Convert.ToDouble(t[j]);
        //        }
        //    }
        //    return trainarr;
        //}

        //Поиск шаблона "арка"
        public static bool isFindPattern(string directoryname, Image<Bgr, byte> image, double threshold = 0.55)
        {
            Image<Bgr, byte> pattern;
            var patterns = Directory.GetFiles(directoryname);
            foreach (var file_pattern in patterns)
            {
                pattern = new Image<Bgr, byte>(file_pattern);
                var result = image.MatchTemplate(pattern, TemplateMatchingType.CcoeffNormed);
                Point[] MAX_Loc, Min_Loc;
                double[] min, max;
                result.MinMax(out min, out max, out Min_Loc, out MAX_Loc);
                if (max[0] > threshold) return true;
            }
            return false;
        }

        //Получение обучающего массива
        public static TrainData GetTrainArray()
        {
            var imgs_onko = Directory.GetFiles(dir_onko);
            var imgs_not_onko = Directory.GetFiles(dir_not_onko);
            if (imgs_onko != null && imgs_not_onko != null)
                if (imgs_onko.Length != 0 && imgs_not_onko.Length != 0)
                {
                    int num_files = imgs_onko.Length /*+ imgs_not_onko.Length*/;
                    int img_area = area;
                    //Mat training_mat = new Mat(num_files, img_area, DepthType.Cv32F, 1);
                    Matrix<float> training_mat = new Matrix<float>(num_files, img_area);
                    int file_num = 0;
                    foreach (var img in imgs_onko)
                    {
                        ImgTo1DArray(img, ref training_mat, file_num++);
                    }
                    //foreach (var img in imgs_not_onko)
                    //{
                    //    ImgTo1DArray(img, ref training_mat, file_num++);
                    //}
                    
                    TrainData trainData = new TrainData(training_mat, Emgu.CV.ML.MlEnum.DataLayoutType.RowSample, GetLabels(num_files, imgs_onko.Length, imgs_not_onko.Length));
                    return trainData;
                }
            return null;
        }

        //Перевод изображения в одномерный массив
        private static void ImgTo1DArray(string imgname, ref /*Mat*/Matrix<float> training_mat, int file_num)
        {
            //Mat img_mat = CvInvoke.Imread(imgname, ImreadModes.Grayscale);
            //int ii = 0; 
            //for (int i = 0; i < img_mat.Rows; i++)
            //{
            //    for (int j = 0; j < img_mat.Cols; j++)
            //    {
            //        SetValue(training_mat, file_num, ii++, GetValue(img_mat, i, j));
            //    }
            //}
            Image<Gray, float> img = new Image<Gray, float>(imgname).Resize(600, 400, Inter.Linear);
            Matrix<float> mat = new Matrix<float>(img.Height, img.Width);
            img.CopyTo(mat);
            int ii = 0;
            for (int i = 0; i < mat.Rows; i++)
            {
                for (int j = 0; j < mat.Cols; j++)
                {
                    training_mat[file_num, ii++] = mat[i, j];
                }
            }
        }

        //Получение данных для машины опорных векторов
        public static /*Mat*/Matrix<float> GetPredictArray(string filename)
        {
            int img_area = area;
            //Mat predict_mat = new Mat(1, img_area, DepthType.Cv32F, 1);
            Matrix<float> predict_mat = new Matrix<float>(1, img_area);
            ImgTo1DArray(filename, ref predict_mat, 0);
            return predict_mat;
        }

        public static float GetValue(Mat mat, int row, int col)
        {
            var value = new float[1];
            Marshal.Copy(mat.DataPointer + (row * mat.Cols + col) * mat.ElementSize, value, 0, 1);
            return value[0];
        }

        public static void SetValue(Mat mat, int row, int col, float value)
        {
            var target = new float[1] { value };
            Marshal.Copy(target, 0, mat.DataPointer + (row * mat.Cols + col) * mat.ElementSize, 1);
        }

        //Получение меток для обучающего массива
        public static Matrix<float> GetLabels(int num_files, int onko_len, int not_onko_len)
        {
            Matrix<float> labels = new Matrix<float>(num_files, 1);
            for (int i=0; i<onko_len; i++)
            {
                labels[i, 0] = 1;
            }
            //for (int i=onko_len; i<not_onko_len; i++)
            //{
            //    labels[i, 0] = -1;
            //}
            return labels;
        }

        //Обучение SVM
        public static SVM TrainSVM(TrainData data)
        {
            SVM svm = new SVM();
            svm.SetKernel(SVM.SvmKernelType.Rbf);
            svm.Type = SVM.SvmType.OneClass;
            //svm.Degree = 50;s
            svm.Nu = 0.7;
            //svm.TrainAuto(data);
            //svm.Save(svm_file);
            return svm;
        }

        //Стартовая функция - загружает машину или обучает ее
        public static void Start()
        {
            if (File.Exists(svm_file))
            {
                machine = new SVM();
                machine.Read(new FileStorage(svm_file, FileStorage.Mode.Read).GetFirstTopLevelNode());
            }
            else
            {
                machine = TrainSVM(GetTrainArray());
            }
        }

        //Ответ машины
        public static string Predict(ref string svm, ref bool isFindPat, ref double area, string filename)
        {
            isFindPat = isFindPattern(dir_arcs, new Image<Bgr, byte>(filename));
            area = CalcCenterProc(filename);
            float svm_num = 0;
            if (machine != null) svm_num = machine.Predict(GetPredictArray(filename));
            svm = svm_num == 1 ? "Онкология" : "Не онкология";
            bool area_flag = area >= 45 && area <= 95, svm_flag = svm_num == 1;
            return area_flag && isFindPat || area_flag && svm_flag || isFindPat && svm_flag ? "Онкология" : "Не онкология";
        }

        ////Обучение машины
        //public static MulticlassSupportVectorMachine<Gaussian> Train(TrainArr trainarr)
        //{
        //    var teacher = new MulticlassSupportVectorLearning<Gaussian>()
        //    {
        //        Learner = (param) => new SequentialMinimalOptimization<Gaussian>()
        //        {
        //            UseKernelEstimation = true
        //        }
        //    };

        //    var machine = teacher.Learn(trainarr.Array, trainarr.Labels);
        //    return machine;
        //}

        ////Распознавание
        //public static int Prediction(MulticlassSupportVectorMachine<Gaussian> machine, double[]input)
        //{
        //    return machine.Decide(input);
        //}


    }
}
