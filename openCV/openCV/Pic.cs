﻿using Emgu.CV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace openCV
{
    public class Pic
    {
        public string Filename { get; set; }
        public int Label { get; set; }
    }

    public class TrainArr
    {
        public int[] Labels { get; set; }
        public double[][] Array { get; set; }
    }
}
