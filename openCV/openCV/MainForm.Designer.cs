﻿namespace openCV
{
    partial class MainForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tcSVM = new System.Windows.Forms.TabControl();
            this.tpPrediction = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.tbAnswer = new System.Windows.Forms.TextBox();
            this.btnPrediction = new System.Windows.Forms.Button();
            this.btnLoadPic = new System.Windows.Forms.Button();
            this.pbPrediction = new System.Windows.Forms.PictureBox();
            this.tpMany = new System.Windows.Forms.TabPage();
            this.btnRec = new System.Windows.Forms.Button();
            this.btnLoadPics = new System.Windows.Forms.Button();
            this.dgvResults = new System.Windows.Forms.DataGridView();
            this.colFileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colResult = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panStandBy = new System.Windows.Forms.Panel();
            this.lbStandBy = new System.Windows.Forms.Label();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tcSVM.SuspendLayout();
            this.tpPrediction.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPrediction)).BeginInit();
            this.tpMany.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvResults)).BeginInit();
            this.panStandBy.SuspendLayout();
            this.SuspendLayout();
            // 
            // tcSVM
            // 
            this.tcSVM.Controls.Add(this.tpPrediction);
            this.tcSVM.Controls.Add(this.tpMany);
            this.tcSVM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcSVM.Location = new System.Drawing.Point(0, 0);
            this.tcSVM.Multiline = true;
            this.tcSVM.Name = "tcSVM";
            this.tcSVM.SelectedIndex = 0;
            this.tcSVM.Size = new System.Drawing.Size(628, 376);
            this.tcSVM.TabIndex = 0;
            // 
            // tpPrediction
            // 
            this.tpPrediction.Controls.Add(this.label1);
            this.tpPrediction.Controls.Add(this.tbAnswer);
            this.tpPrediction.Controls.Add(this.btnPrediction);
            this.tpPrediction.Controls.Add(this.btnLoadPic);
            this.tpPrediction.Controls.Add(this.pbPrediction);
            this.tpPrediction.Location = new System.Drawing.Point(4, 22);
            this.tpPrediction.Name = "tpPrediction";
            this.tpPrediction.Padding = new System.Windows.Forms.Padding(3);
            this.tpPrediction.Size = new System.Drawing.Size(620, 350);
            this.tpPrediction.TabIndex = 1;
            this.tpPrediction.Text = "Проверить изображение";
            this.tpPrediction.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(453, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Ответ";
            // 
            // tbAnswer
            // 
            this.tbAnswer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbAnswer.Location = new System.Drawing.Point(450, 77);
            this.tbAnswer.Name = "tbAnswer";
            this.tbAnswer.ReadOnly = true;
            this.tbAnswer.Size = new System.Drawing.Size(162, 20);
            this.tbAnswer.TabIndex = 3;
            // 
            // btnPrediction
            // 
            this.btnPrediction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrediction.Location = new System.Drawing.Point(450, 35);
            this.btnPrediction.Name = "btnPrediction";
            this.btnPrediction.Size = new System.Drawing.Size(162, 23);
            this.btnPrediction.TabIndex = 2;
            this.btnPrediction.Text = "Распознать";
            this.btnPrediction.UseVisualStyleBackColor = true;
            this.btnPrediction.Click += new System.EventHandler(this.btnPrediction_Click);
            // 
            // btnLoadPic
            // 
            this.btnLoadPic.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLoadPic.Location = new System.Drawing.Point(450, 6);
            this.btnLoadPic.Name = "btnLoadPic";
            this.btnLoadPic.Size = new System.Drawing.Size(162, 23);
            this.btnLoadPic.TabIndex = 1;
            this.btnLoadPic.Text = "Загрузить";
            this.btnLoadPic.UseVisualStyleBackColor = true;
            this.btnLoadPic.Click += new System.EventHandler(this.btnLoadPic_Click);
            // 
            // pbPrediction
            // 
            this.pbPrediction.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbPrediction.BackColor = System.Drawing.SystemColors.Control;
            this.pbPrediction.Location = new System.Drawing.Point(0, 0);
            this.pbPrediction.Name = "pbPrediction";
            this.pbPrediction.Size = new System.Drawing.Size(444, 354);
            this.pbPrediction.TabIndex = 0;
            this.pbPrediction.TabStop = false;
            // 
            // tpMany
            // 
            this.tpMany.Controls.Add(this.btnRec);
            this.tpMany.Controls.Add(this.btnLoadPics);
            this.tpMany.Controls.Add(this.dgvResults);
            this.tpMany.Location = new System.Drawing.Point(4, 22);
            this.tpMany.Name = "tpMany";
            this.tpMany.Padding = new System.Windows.Forms.Padding(3);
            this.tpMany.Size = new System.Drawing.Size(620, 350);
            this.tpMany.TabIndex = 2;
            this.tpMany.Text = "Проверить несколько изображений";
            this.tpMany.UseVisualStyleBackColor = true;
            // 
            // btnRec
            // 
            this.btnRec.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRec.Location = new System.Drawing.Point(450, 35);
            this.btnRec.Name = "btnRec";
            this.btnRec.Size = new System.Drawing.Size(162, 23);
            this.btnRec.TabIndex = 4;
            this.btnRec.Text = "Распознать";
            this.btnRec.UseVisualStyleBackColor = true;
            this.btnRec.Click += new System.EventHandler(this.btnRec_Click);
            // 
            // btnLoadPics
            // 
            this.btnLoadPics.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLoadPics.Location = new System.Drawing.Point(450, 6);
            this.btnLoadPics.Name = "btnLoadPics";
            this.btnLoadPics.Size = new System.Drawing.Size(162, 23);
            this.btnLoadPics.TabIndex = 3;
            this.btnLoadPics.Text = "Загрузить";
            this.btnLoadPics.UseVisualStyleBackColor = true;
            this.btnLoadPics.Click += new System.EventHandler(this.btnLoadPics_Click);
            // 
            // dgvResults
            // 
            this.dgvResults.AllowUserToAddRows = false;
            this.dgvResults.AllowUserToDeleteRows = false;
            this.dgvResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvResults.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colFileName,
            this.colResult,
            this.Column1,
            this.Column2,
            this.Column3});
            this.dgvResults.Location = new System.Drawing.Point(0, 0);
            this.dgvResults.Name = "dgvResults";
            this.dgvResults.ReadOnly = true;
            this.dgvResults.Size = new System.Drawing.Size(440, 350);
            this.dgvResults.TabIndex = 0;
            // 
            // colFileName
            // 
            this.colFileName.HeaderText = "Имя файла";
            this.colFileName.Name = "colFileName";
            this.colFileName.ReadOnly = true;
            // 
            // colResult
            // 
            this.colResult.HeaderText = "Полученный результат";
            this.colResult.Name = "colResult";
            this.colResult.ReadOnly = true;
            // 
            // panStandBy
            // 
            this.panStandBy.Controls.Add(this.lbStandBy);
            this.panStandBy.Location = new System.Drawing.Point(0, 0);
            this.panStandBy.Name = "panStandBy";
            this.panStandBy.Size = new System.Drawing.Size(10, 10);
            this.panStandBy.TabIndex = 1;
            this.panStandBy.Visible = false;
            // 
            // lbStandBy
            // 
            this.lbStandBy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lbStandBy.AutoSize = true;
            this.lbStandBy.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbStandBy.Location = new System.Drawing.Point(160, -11);
            this.lbStandBy.Name = "lbStandBy";
            this.lbStandBy.Size = new System.Drawing.Size(319, 44);
            this.lbStandBy.TabIndex = 0;
            this.lbStandBy.Text = "Пожалуйста подождите \r\nВыполняется подготовка к запуску";
            this.lbStandBy.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Площадь";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Есть паттерн";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "SVM";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(628, 376);
            this.Controls.Add(this.panStandBy);
            this.Controls.Add(this.tcSVM);
            this.Name = "MainForm";
            this.Text = "Диагностика заболеваний";
            this.tcSVM.ResumeLayout(false);
            this.tpPrediction.ResumeLayout(false);
            this.tpPrediction.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPrediction)).EndInit();
            this.tpMany.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvResults)).EndInit();
            this.panStandBy.ResumeLayout(false);
            this.panStandBy.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tcSVM;
        private System.Windows.Forms.TabPage tpPrediction;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbAnswer;
        private System.Windows.Forms.Button btnPrediction;
        private System.Windows.Forms.Button btnLoadPic;
        private System.Windows.Forms.PictureBox pbPrediction;
        private System.Windows.Forms.TabPage tpMany;
        private System.Windows.Forms.Button btnRec;
        private System.Windows.Forms.Button btnLoadPics;
        private System.Windows.Forms.DataGridView dgvResults;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFileName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colResult;
        private System.Windows.Forms.Panel panStandBy;
        private System.Windows.Forms.Label lbStandBy;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
    }
}

