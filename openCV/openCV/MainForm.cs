﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace openCV
{
    public partial class MainForm : Form
    {
        //List<Pic> pictures;
        //TrainArr trainarr;
        //MulticlassSupportVectorMachine<Gaussian> machine;
        string filenameprediction;
        String[] imgfilepaths;

        public MainForm()
        {
            InitializeComponent();
            panStandBy.Size = Size;
            panStandBy.Visible = true;
            PatRec.Start();
            panStandBy.Visible = false;
            //pictures = new List<Pic>();
            //trainarr = new TrainArr();
            isEmpty();
        }

        //private void btnLoad_Click(object sender, EventArgs e)
        //{
        //    OpenFileDialog ofd = new OpenFileDialog();
        //    ofd.Filter = "Image | *.bmp;*.jpg;*.gif;*.png";
        //    ofd.Multiselect = true;
        //    ofd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
        //    if (ofd.ShowDialog() == DialogResult.OK)
        //    {
        //        pictures.Clear();
        //        var filepaths = ofd.FileNames;
        //        foreach (var file in filepaths)
        //        {
        //            var name = Path.GetFileNameWithoutExtension(file).Split('_');
        //            pictures.Add(new Pic() { Filename = file, Label = GetLabel(name[0]) });
        //        }
        //        ShowPics();
        //        isEmpty();
        //    }
        //}

        void isEmpty()
        {
            if (pbPrediction.Image == null)
                btnPrediction.Enabled = false;
            else
                btnPrediction.Enabled = true;

            if (imgfilepaths != null)
                btnRec.Enabled = imgfilepaths.Length != 0;
            else
                btnRec.Enabled = false;
        }

        //int GetLabel(string name)
        //{
        //    switch (name)
        //    {
        //        case "Воспалительное": return 0;
        //        case "Нефрология": return 1;
        //        case "Онкология": return 2;
        //        case "Панкреатогенные": return 3;
        //        case "Пневмонэктомия": return 4;
        //        case "Посттравмат": return 5;
        //        case "Туберкулез": return 6;
        //        case "Хилоторакс": return 7;
        //        case "Цирроз печени": return 8;
        //        default: return -1;
        //    }
        //}

        //void ShowPics()
        //{
        //    //panelImage.Controls.Clear();
        //    int picY = 0, picX = -110, col = 0, row = 0;
        //    for (int i = 0; i < /*pictures.Count*/30; i++)
        //    {
        //        PictureBox pic = new PictureBox();
        //        if (col < 4)
        //        {
        //            picX = picX + 120;
        //            picY = 10 + row * 110;
        //            col++;
        //        }
        //        else
        //        {
        //            row++;
        //            col = 1;
        //            picX = 10;
        //            picY = 10 + row * 110;
        //        }

        //        pic.Location = new Point(picX, picY);
        //        pic.Size = new System.Drawing.Size(100, 100);
        //        pic.SizeMode = PictureBoxSizeMode.Zoom;

        //        Image<Bgr, byte> img = new Image<Bgr, byte>(pictures[i].Filename);
        //        pic.Image = img.ToBitmap();

        //        //panelImage.Controls.Add(pic);
        //    }

        //}

        //private void btnTrain_Click(object sender, EventArgs e)
        //{
        //    if (pictures.Count != 0)
        //    {
        //        //trainarr = PatRec.SetTrainArray(pictures);
        //        //machine = PatRec.Train(trainarr);
        //        MessageBox.Show("OK");
        //    }
        //}

        private void btnLoadPic_Click(object sender, EventArgs e)
        {
            pbPrediction.SizeMode = PictureBoxSizeMode.Zoom;
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Image | *.bmp;*.jpg;*.gif;*.png";
            ofd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                pbPrediction.Image = new Bitmap(ofd.FileName);
                filenameprediction = ofd.FileName;
            }
            isEmpty();
        }

        private void btnPrediction_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(filenameprediction))
            {
                string svm = "";
                bool pat = false;
                double area = 0;
                tbAnswer.Text = PatRec.Predict(ref svm, ref pat, ref area, filenameprediction);
            }
        }

        private void btnLoadPics_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Image | *.bmp;*.jpg;*.gif;*.png";
            ofd.Multiselect = true;
            ofd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                imgfilepaths = ofd.FileNames;
                //foreach (var file in filepaths)
                //{
                //    //var name = Path.GetFileNameWithoutExtension(file).Split('_');
                //    //pictures.Add(new Pic() { Filename = file, Label = GetLabel(name[0]) });
                //    imgfilepaths.Add(file);
                //}
                isEmpty();
            }
        }

        private void btnRec_Click(object sender, EventArgs e)
        {
            string svm = "";
            bool pat = false;
            double area = 0;
            foreach (var file in imgfilepaths)
            {
                dgvResults.Rows.Add(Path.GetFileName(file), PatRec.Predict(ref svm, ref pat, ref area, file), area.ToString(), pat.ToString(), svm);
            }
        }

        //private void btnThreshold_Click(object sender, EventArgs e)
        //{
        //    if (!String.IsNullOrEmpty(filenameprediction))
        //    {
        //        var image = new Image<Bgr, Byte>(filenameprediction).Resize(0.2, Emgu.CV.CvEnum.Inter.Linear);
        //        var bin_image = PatRec.Binary(new Image<Bgr, Byte>(filenameprediction).Resize(0.2, Emgu.CV.CvEnum.Inter.Linear), gray_threshold: 155, gaussian: 21);
        //        pbPrediction.Image = bin_image.ToBitmap();
        //        SimpleBlobDetectorParams param = new SimpleBlobDetectorParams();
        //        param.FilterByColor = true;
        //        param.blobColor = 0;

        //        param.FilterByArea = true;
        //        param.MinArea = 10;
        //        param.MaxArea = 10000;

        //        param.MinDistBetweenBlobs = 1;

        //        //param.FilterByCircularity = true;
        //        //param.MinCircularity = 0.6F;
        //        //param.MaxCircularity = 1;

        //        //param.FilterByConvexity = true;
        //        //param.MinConvexity = 0.5F;
        //        //param.MaxConvexity = 1;

        //        //param.FilterByInertia = true;
        //        //param.MinInertiaRatio = 0.5F;
        //        //param.MaxInertiaRatio = 1;

        //        SimpleBlobDetector bd = new SimpleBlobDetector(param);
        //        //CvBlobs blobs = new CvBlobs();

        //        var keypoints = bd.Detect(bin_image);
        //        //Features2DToolbox.DrawKeypoints(image, new VectorOfKeyPoint(keypoints), image, new Bgr(Color.Red));
        //        //pbPrediction.Image = image.ToBitmap();

        //        PatRec.Segmentation(bin_image, image);
        //        pbPrediction.Image = image.ToBitmap();


        //        //pbPrediction.Image = bd.DrawBlobs(bin_image, blobs, CvBlobDetector.BlobRenderType.Default, 1).ToBitmap();

        //        //Gray cannyThreshold = new Gray(180);
        //        //Gray cannyThresholdLinking = new Gray(120);
        //        //Gray circleAccumulatorThreshold = new Gray(60);

        //        //CircleF[] circles = bin_image.HoughCircles(
        //        //    cannyThreshold,
        //        //    circleAccumulatorThreshold,
        //        //    4.0, //Resolution of the accumulator used to detect centers of the circles
        //        //    100, //min distance 
        //        //    500, //min radius
        //        //    10000 //max radius
        //        //    )[0]; //Get the circles from the first channel
        //        //foreach (var cir in circles)
        //        //{
        //        //    image.Draw(cir, new Bgr(Color.Red));
        //        //}
        //        //pbPrediction.Image = image.ToBitmap();
        //    }
        //}
    }
}
